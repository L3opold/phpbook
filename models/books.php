<?php

use Phalcon\Mvc\Collection as Collection;

/**
 * @author  Léopold Jacquot
 */
class Books extends Collection {

    /**
     * @var string $title
     */
    public $title;

    /**
     * @return bool
     */
    public function validation() {
        $this->validate(
            new \Phalcon\Mvc\Model\Validator\StringLength(
                [
                    'field' => 'title',
                    'max' => 50,
                    'min' => 2,
                    'messageMaximum' => 'We don\'t like really long names',
                    'messageMinimum' => 'We want the full name'
                ]
            )
        );

        return $this->validationHasFailed() != true;
    }
}