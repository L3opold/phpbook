<?php

use Phalcon\Mvc\View;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\Collection\Manager as CollectionManager;

$di = new FactoryDefault();

/**
 * Sets the view component
 */
$di['view'] = function () use ($config) {
    $view = new View();
    $view->setViewsDir($config->application->viewsDir);

    return $view;
};

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di['url'] = function () use ($config) {
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
};

/** Simple database connection to localhost
 *
 */
$di['mongo'] = function() {
    $mongo = new MongoClient();
    return $mongo->selectDb("mongoBook");
};

//Collection manager
$di['collectionManager'] = function() {
    return new CollectionManager();
};