<?php

return new \Phalcon\Config(array(
    'database' => array(
        'adapter'    => 'mongo',
        'host'       => 'localhost',
        'username'   => '',
        'password'   => '',
        'dbname'     => 'mongoBook',
    ),
    'application' => array(
        'modelsDir'      => __DIR__ . '/../models/',
        'viewsDir'      => __DIR__ . '/../views/',
        'baseUri'        => '/',
    )
));
