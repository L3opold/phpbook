<?php use Phalcon\Http\Response;

/**
 * Add your routes here
 */

// Retrieves all books
$app->get('/books', function () use ($app) {
    echo json_encode(Books::find());
});

// Retrieves books based on primary key
$app->get('/books/{id}', function ($id) {
    $book = Books::findById($id);

    if ($book == false) {
        echo json_encode(['status' => 'Error']);
    } else {
        echo json_encode(['status' => 'Ok', 'book' => $book]);
    }
});

// Adds a new books
$app->post('/books', function () {
    $book       = new Books();
    $book->title = isset($_POST['title']) ? $_POST['title'] : null;

    if ($book->save() == false) {
        echo json_encode(['status' => 'Error']);
    } else {
        echo json_encode(['status' => 'Ok']);
    }

});

// Updates books based on primary key
$app->put('/books/{id}', function ($id) {
    $book = Books::findById($id);
    if ($book != false) {
        $book->title = isset($_POST['title']) ? $_POST['title'] : null;

        if ($book->save() == false) {
            echo json_encode(['status' => 'Error']);
        } else {
            echo json_encode(['status' => 'Ok']);
        }
    }
});

// Deletes books based on primary key
$app->delete('/books/{id}', function ($id) {
    $book = Books::findById($id);
    if ($book != false) {
        if ($book->delete() == false) {
            echo json_encode(['status' => 'Error']);
        } else {
            echo json_encode(['status' => 'Ok']);
        }
    }
});

/**
 * Not found handler
 */
$app->notFound(function () use ($app) {
    $app->response->setStatusCode(404, "Not Found")->sendHeaders();
    echo $app['view']->getRender(null, '404');
});
